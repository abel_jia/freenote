package cn.jfstu.freenote.task;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * @author 贾俊峰
 * @version 1.0
 * @date 2016/11/25 13:08
 */
@Component
public class TestTask {
    private static final Logger logger = LoggerFactory.getLogger(TestTask.class);

    @Scheduled(cron = "0/10 * * * * ?")
    private void taskRun(){
        logger.info("CustomTask run ...");
    }
}
