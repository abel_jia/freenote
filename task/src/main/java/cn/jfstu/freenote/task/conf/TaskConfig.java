package cn.jfstu.freenote.task.conf;

import cn.jfstu.freenote.pojo.DatabaseConfig;
import cn.jfstu.freenote.pojo.RuntimeEnvironment;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @author 贾俊峰
 * @version 1.0
 * @date 2016/11/29 10:13
 */
@ComponentScan(basePackages = "cn.jfstu.freenote.task", useDefaultFilters = false, includeFilters = {
        @ComponentScan.Filter(type = FilterType.ANNOTATION, value = {Component.class})
})
@Configuration
public class TaskConfig {
    @Resource
    private RuntimeEnvironment runtimeEnvironment;

    @Bean
    public DatabaseConfig databaseConfig() {
        return new DatabaseConfig(runtimeEnvironment);
    }
}
