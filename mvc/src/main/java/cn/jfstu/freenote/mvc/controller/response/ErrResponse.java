package cn.jfstu.freenote.mvc.controller.response;

/**
 * 异常响应报文
 * @author 贾俊峰
 * @version 1.0
 * @date 2016/9/1 12:01
 */
public class ErrResponse extends BaseResponse {
    private static final long serialVersionUID = -3799679093908414914L;

    private String code;
    private String msg;

    public ErrResponse(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public String getCode() {
        return code;
    }
    public void setCode(String code) {
        this.code = code;
    }
    public String getMsg() {
        return msg;
    }
    public void setMsg(String msg) {
        this.msg = msg;
    }
}
