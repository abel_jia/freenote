package cn.jfstu.freenote.mvc.conf;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.Order;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

/**
 * servlet初始化
 *
 * @author 贾俊峰
 * @version 1.0
 * @date 2016/11/25 14:27
 */
@Order(2)
public class ServletInitialize extends AbstractAnnotationConfigDispatcherServletInitializer {
    private static final Logger logger = LoggerFactory.getLogger(ServletInitialize.class);

    @Override
    protected Class<?>[] getRootConfigClasses() {
        logger.info("加载应用上下文配置");
        return new Class<?>[]{AppConfig.class};
    }

    @Override
    protected Class<?>[] getServletConfigClasses() {
        logger.info("加载web上下文配置");
        return new Class<?>[]{MvcConfig.class};
    }

    @Override
    protected String[] getServletMappings() {
        logger.info("设置前端控制器映射路径");
        return new String[]{"/"};
    }
}
