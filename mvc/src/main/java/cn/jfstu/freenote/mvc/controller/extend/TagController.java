package cn.jfstu.freenote.mvc.controller.extend;

import cn.jfstu.freenote.entity.extend.Tag;
import cn.jfstu.freenote.mvc.controller.base.BaseController;
import cn.jfstu.freenote.service.extend.TagService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.List;

/**
 * 标签控制层
 *
 * @author 贾俊峰
 * @version 1.0
 * @date 2016/12/30 18:05
 */
@Controller
@RequestMapping("/tag")
public class TagController extends BaseController {
    @Resource
    private TagService tagService;

    /**
     * 获取标签列表
     *
     * @return 标签列表
     */
    @RequestMapping("/list")
    @ResponseBody
    public List<Tag> getList() {
        return tagService.getList(null);
    }
}
