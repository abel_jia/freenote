package cn.jfstu.freenote.mvc.conf;

import cn.jfstu.freenote.pojo.RuntimeEnvironment;
import cn.jfstu.freenote.service.conf.ServiceConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * @author 贾俊峰
 * @version 1.0
 * @date 2016/11/29 10:37
 */
@Import({ServiceConfig.class})
@Configuration
public class AppConfig {
    private static final Logger logger = LoggerFactory.getLogger(AppConfig.class);
    @Bean
    public RuntimeEnvironment getRuntimeEnvironment() {
        logger.info("运行环境：开发环境");
        return RuntimeEnvironment.DEVELOPMENT;
    }

}
