package cn.jfstu.freenote.mvc.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 自定义主解：禁用登录检查
 * @author 贾俊峰
 * @version 1.0
 * @date 2016/9/27 16:50
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface DisableLoginCheck {
}
