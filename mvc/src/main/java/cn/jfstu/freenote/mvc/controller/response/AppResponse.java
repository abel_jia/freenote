package cn.jfstu.freenote.mvc.controller.response;

/**
 * 应用响应报文
 * @author 贾俊峰
 * @version 1.0
 * @date 2016/9/1 12:01
 */
public class AppResponse extends BaseResponse {
    private static final long serialVersionUID = -2165278372430722841L;

    private Object data;
    private String code;

    public AppResponse() {
        this(null);
    }
    public AppResponse(Object obj) {
        this.code = "0";
        this.data = obj;
    }
    public String getCode() {
        return code;
    }

    public Object getData() {
        return this.data;
    }
}
