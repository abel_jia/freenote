package cn.jfstu.freenote.mvc.controller.extend;

import cn.jfstu.freenote.entity.extend.User;
import cn.jfstu.freenote.mvc.annotation.DisableLoginCheck;
import cn.jfstu.freenote.mvc.assist.Session;
import cn.jfstu.freenote.mvc.controller.base.BaseController;
import cn.jfstu.freenote.service.extend.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

import javax.annotation.Resource;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Locale;

/**
 * 主控制器
 *
 * @author 贾俊峰
 * @version 1.0
 * @date 2016/11/24 16:28
 */
@Controller
public class MainController extends BaseController {
    @Resource
    private UserService userService;

    /**
     * 请求网站根目录
     * 根据配置返回：主页，笔记页，博客页，登录页等
     */
    @RequestMapping("/")
    @DisableLoginCheck
    public String root() {
        // todo 根据配置返回：主页，笔记页，博客页，登录页等，目前只返回index页
        return "main/index";
    }

    /**
     * 请求主页
     *
     * @return index页
     */
    @RequestMapping("/index")
    @DisableLoginCheck
    public String index() {
        return "main/index";
    }

    /**
     * 请求注册页
     *
     * @return 注册页
     */
    @RequestMapping("/register")
    @DisableLoginCheck
    public String register() {
        return "main/register";
    }

    /**
     * 请求登录页
     *
     * @return 登录页
     */
    @RequestMapping("/login")
    @DisableLoginCheck
    public String login() {
        return "main/login";
    }

    /**
     * 请求找回密码页
     *
     * @return 找回密码页
     */
    @RequestMapping("/findPassword")
    @DisableLoginCheck
    public String findPassword() {
        return "main/findPassword";
    }


    /**
     * 请求笔记页
     *
     * @return 笔记页
     */
    @RequestMapping("/note")
    public String note() {
        return "note/index";
    }

    /**
     * 请求博客页
     *
     * @return 博客页
     */
    @RequestMapping("/blog")
    @DisableLoginCheck
    public String blog() {
        return "blog/index";
    }

    /**
     * 请求试用页
     *
     * @return 试用页
     */
    @RequestMapping("/demo")
    @DisableLoginCheck
    public String demo(HttpSession session) {
        User user = userService.getByUsername("demo");
        session.setAttribute(Session.LOGIN_USER, user);
        return "note/index";
    }

    /**
     * 设置页面语言
     *
     * @param session  用户会话
     * @param language 语言
     */
    @RequestMapping("/lang")
    @DisableLoginCheck
    @ResponseBody
    public void lang(HttpServletResponse response, HttpSession session, String language) {
        session.setAttribute(SessionLocaleResolver.LOCALE_SESSION_ATTRIBUTE_NAME, new Locale(language));
        response.addCookie(new Cookie("language", language));
    }

}
