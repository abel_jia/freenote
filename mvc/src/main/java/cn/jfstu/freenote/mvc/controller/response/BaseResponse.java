package cn.jfstu.freenote.mvc.controller.response;

import java.io.Serializable;

/**
 * 响应报文基类
 * @author 贾俊峰
 * @version 1.0
 * @date 2016/9/1 12:01
 */
public abstract class BaseResponse implements Serializable {
    private static final long serialVersionUID = 9095334956733662001L;
}
