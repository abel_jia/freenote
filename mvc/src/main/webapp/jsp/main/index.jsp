<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:include page="/jsp/common/common.jsp"/>
<html>
<head>
    <title>FreeNote</title>
</head>
<link rel="stylesheet" href="<c:url value="/css/main/index.css"/>">
<body>
<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-links">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">
                <img id="logo" style="height: 40px; margin-top: -10px;" src="<c:url value="/img/logo.png"/>" alt="">
            </a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="navbar-links">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="#"><spring:message code="index.home"/></a></li>
                <li><a href="#"><spring:message code="index.bbs"/></a></li>
                <li><a href="#"><spring:message code="index.explore"/></a></li>
                <li><a href="#"><spring:message code="index.app"/></a></li>
                <li><a href="#"><spring:message code="index.donate"/></a></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">language <span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu" id="languages">
                        <li><a href="#" data-lang="en_US">English</a></li>
                        <li><a href="#" data-lang="zh_CN">简体中文</a></li>
                        <li><a href="#" data-lang="zh_TW">繁體中文</a></li>
                    </ul>
                </li>
                <script>
                    var language = $.cookie("language");
                    var $languages = $("#languages");
                    $languages.find("a[data-lang="+language+"]").parent().addClass("active");
                    $languages.find("a").on("click", function () {
                        var lang = $(this).attr("data-lang");
                        $.post("<c:url value="/lang"/>", {language: lang}, function () {
                            store.set("language", lang);
                            window.location.reload();
                        });
                    });
                </script>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>

<section class="text-center">
    <div>
        <h2 style="color: #fff;"><spring:message code="index.moto"/></h2>
        <p><spring:message code="index.moto2"/></p>
        <p><spring:message code="index.moto3"/></p>
    </div>
    <div id="buttons">
        <a href="<c:url value="/register"/>" class="btn btn-primary"><spring:message code="index.register"/></a>
        <a href="<c:url value="/login"/>" class="btn btn-default"><spring:message code="index.login"/></a>
        <a href="<c:url value="/demo"/>" class="btn btn-default"><spring:message code="index.try"/></a>
    </div>
    <div class="text">
        <img src="<c:url value="/img/temp/writting.png"/> " alt="">
    </div>
</section>
<div class="container" id="aboutLeanote">
    <h2 class="text-center"><spring:message code="index.aboutFreeNote"/></h2>
    <div class="row">
        <div class="col-md-3">
            <h3><spring:message code="index.knowledge"/></h3>
            <p><spring:message code="index.knowledgeInfo"/></p>
        </div>
        <div class="col-md-3">
            <h3><spring:message code="index.blog"/></h3>
            <p><spring:message code="index.blogInfo"/></p>
        </div>
        <div class="col-md-3">
            <h3><spring:message code="index.share"/></h3>
            <p><spring:message code="index.shareInfo"/></p>
        </div>
        <div class="col-md-3">
            <h3><spring:message code="index.cooperation"/></h3>
            <p><spring:message code="index.cooperationInfo"/></p>
        </div>
    </div>
</div>
<div class="copyright text-center">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <span>Copyright ©2016 <a href="http://www.jfstu.cn/">峰言疯语</a> All rights reserved.</span> |
                <span><a href="http://www.miitbeian.gov.cn/" target="_blank">皖ICP备16009659号</a></span>
            </div>
        </div>
    </div>
</div>
</body>
</html>
