<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:include page="/jsp/common/common.jsp"/>
<html>
<head>
    <title>找回密码</title>
    <link rel="stylesheet" href="<c:url value="/css/main/common.css"/>">
</head>
<body>
<jsp:include page="header.jsp"/>
<section>
    <div id="formBox">
        <div class="box-header">找回密码</div>
        <div class="box-content">
            <form role="form">
                <div class="form-group">
                    <label for="email">Email地址</label>
                    <div class="input-group">
                        <input type="email" class="form-control" id="email">
                        <div class="input-group-btn">
                            <button class="btn btn-success disabled">发送邮件</button>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="checkCode">验证码</label>
                    <input type="password" class="form-control" id="checkCode">
                </div>
                <div class="form-group">
                    <label for="password">新密码</label>
                    <input type="password" class="form-control" id="password">
                </div>
                <div class="form-group">
                    <label for="confirmPassword">再次输入新密码</label>
                    <input type="password" class="form-control" id="confirmPassword">
                </div>
                <button type="button" class="btn btn-primary disabled">找回密码</button>
                <hr>
                <p class="text-center help-block">想起来了？</p>
                <a href="<c:url value="/login"/>" class="btn btn-default btn-block">登录</a>
            </form>
        </div>
    </div>
</section>
<jsp:include page="footer.jsp"/>
</body>
</html>
