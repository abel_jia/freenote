<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:include page="/jsp/common/common.jsp"/>
<html>
<head>
    <title><spring:message code="register.title"/></title>
    <link rel="stylesheet" href="<c:url value="/css/main/common.css"/>">
    <script src="<c:url value="/js/main/register.js"/>"></script>
</head>
<body>
<jsp:include page="header.jsp"/>
<section>
    <div id="formBox">
        <div class="box-header"><spring:message code="register.title"/></div>
        <div class="box-content">
            <form role="form" method="post" action="<c:url value="/auth/register"/>" >
                <div class="form-group">
                    <label for="email" class="control-label"><spring:message code="register.email"/></label>
                    <input type="email" class="form-control" id="email" name="email" required>
                </div>
                <div class="form-group">
                    <label for="password" class="control-label"><spring:message code="register.password"/></label>
                    <input type="password" class="form-control" id="password" name="password" required minlength="6">
                </div>
                <div class="form-group">
                    <label for="confirmPassword" class="control-label"><spring:message code="register.confirmPassword"/></label>
                    <input type="password" class="form-control" id="confirmPassword" name="confirmPassword" required equalTo="#password">
                </div>
                <button type="button" id="submit" class="btn btn-primary"><spring:message code="register.submit"/></button>
                <button type="reset" id="reset" class="btn btn-default"><spring:message code="register.reset"/></button>
                <button type="button" id="cancel" class="btn btn-default"><spring:message code="register.cancel"/></button>
                <hr>
                <p class="text-center help-block"><spring:message code="register.haveAccount"/></p>
                <a href="<c:url value="/login"/>" class="btn btn-default btn-block"><spring:message code="register.login"/></a>
            </form>
        </div>
    </div>
</section>
<jsp:include page="footer.jsp"/>
</body>
</html>
