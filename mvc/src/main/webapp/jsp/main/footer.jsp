<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<link rel="stylesheet" href="<c:url value="/css/main/footer.css"/>">
<footer class="text-center">
    <p><a href="<c:url value="/index"/>"><spring:message code="footer.home"/></a></p>
    <p>jfstu.cn &copy; 2016</p>
</footer>
