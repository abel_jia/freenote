<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:include page="/jsp/common/common.jsp"/>
<html>
<head>
    <title><spring:message code="login.title"/></title>
    <link rel="stylesheet" href="<c:url value="/css/main/common.css"/>">
    <script src="<c:url value="/js/main/login.js"/>"></script>
</head>
<body>
<jsp:include page="header.jsp"/>
<section>
    <div id="formBox">
        <div class="box-header"><spring:message code="login.title"/></div>
        <div class="box-content">
            <form role="form" action="<c:url value="/auth/login"/>" method="post">
                <div class="form-group">
                    <label for="username" class="control-label"><spring:message code="login.username"/></label>
                    <input type="text" class="form-control" id="username" name="username" required>
                </div>
                <div class="form-group">
                    <label for="password" class="control-label"><spring:message code="login.password"/></label>
                    <input type="password" class="form-control" id="password" name="password" required>
                </div>
                <button type="button" id="login" class="btn btn-primary"><spring:message code="login.login"/></button>
                <a href="<c:url value="/findPassword"/>" class="pull-right"><spring:message code="login.forgetPassword"/></a>
                <hr>
                <p class="text-center help-block"><spring:message code="login.noAccount"/></p>
                <a href="<c:url value="/register"/>" class="btn btn-default btn-block"><spring:message code="login.register"/></a>
                <span><spring:message code="login.or"/></span>
                <a href="<c:url value="/demo"/>" class="btn btn-default btn-block"><spring:message code="login.try"/></a>
            </form>
        </div>
    </div>
</section>
<jsp:include page="footer.jsp"/>
</body>
</html>
