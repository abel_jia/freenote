<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";

    response.setHeader("Pragma", "no-cache");
    response.setHeader("Cache-Control", "no-cache");
    response.setDateHeader("Expires", 0);
%>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="shortcut icon" href="<c:url value="/img/favicon.ico"/>" type="image/x-icon"/>
<link rel="stylesheet" href="<c:url value="/lib/bootstrap/css/bootstrap.min.css"/> ">
<link rel="stylesheet" href="<c:url value="/lib/font-awesome/css/font-awesome.min.css"/>">
<script src="<c:url value="/lib/jquery/jquery-3.1.1.min.js"/> "></script>
<script src="<c:url value="/lib/bootstrap/js/bootstrap.min.js"/> "></script>
<script src="<c:url value="/lib/store.js/store+json2.min.js"/>"></script>
<script src="<c:url value="/lib/jquery-cookie/jquery.cookie.js"/>"></script>
<script src="<c:url value="/lib/jquery-validation/jquery.validate.min.js"/>"></script>
<script src="<c:url value="/lib/jquery-validation/localization/messages_zh.js"/>"></script>
<script src="<c:url value="/js/common/common.js"/>"></script>
<style>
    body {
        font-family: "Helvetica Neue", Helvetica, Microsoft Yahei, Hiragino Sans GB, WenQuanYi Micro Hei, sans-serif;
    }
</style>
<base href="<%=basePath%>">
<script>
    const LOGIN_PAGE="<c:url value="/login"/>";
    const NOTE_PAGE="<c:url value="/note"/>";
</script>