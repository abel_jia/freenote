<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:include page="/jsp/common/common.jsp"/>
<html>
<head>
    <title>FreeNote,自由笔记</title>
    <link rel="stylesheet" href="<c:url value="/css/note/note.css"/>">
    <link rel="stylesheet" href="<c:url value="/css/note/theme/default.css"/>">
    <script src="<c:url value="/js/note/index.js"/>"></script>
</head>
<body>
<div id="header">
    <div class="pull-left">
        <a href="#"><img id="logo" src="<c:url value="/img/logo.png"/>" alt=""></a>
    </div>
    <div class="pull-left">
        <input type="text" id="searchNoteInput" class="form-control" placeholder="搜索">
    </div>
    <div class="pull-left">
        <button class="btn btn-default">新建笔记</button>
    </div>
    <div class="pull-left">
        <p>- 我的笔记</p>
    </div>
    <div id="user" class="dropdown pull-right">
        <a class="dropdown-toggle" data-toggle="dropdown">
            <img id="avatar" src="<c:url value="/img/temp/default_avatar.png"/>" alt="头像">
            <span>admin</span>
            <span class="caret"></span>
        </a>
        <ul class="dropdown-menu dropdown-menu-right" role="menu">
            <li><a href="#"><span class="glyphicon glyphicon-user"></span> 个人中心</a></li>
            <li><a href="#"><span class="glyphicon glyphicon-th-large"></span> 主题设置</a></li>
            <li><a href="#"><span class="glyphicon glyphicon-book"></span> 博客设置</a></li>
            <li><a href="#"><span class="glyphicon glyphicon-cog"></span> 管理后台</a></li>
            <li class="divider"></li>
            <li><a href="#"><span class="glyphicon glyphicon-log-out"></span> 登出</a></li>
        </ul>
    </div>
    <div id="links" class="pull-right">
        <a>写作模式</a>
        <a>我的博客</a>
        <a>探索</a>
    </div>
</div>
<div id="mainContainer">
    <div id="notebooks">
        <div class="menu-group">
            <div class="menu open">
                <div class="menu-heading">
                    <span class="menu-icon fa fa-book"></span>
                    <span class="menu-title">笔记本</span>
                    <span id="addNotebookPlus" class="fa fa-plus"></span>
                </div>
                <div class="menu-body">
                    <ul id="notebookList">
                        <li class="search">
                            <input id="searchNotebookInput" type="text" class="form-control input-sm" placeholder="搜索笔记本">
                        </li>
                        <li class="notebook">
                            <div class="info">
                                <span class="icon empty"></span><span class="name">最近</span>
                            </div>
                        </li>
                        <li class="notebook">
                            <div class="info">
                                <span class="icon fa fa-angle-right"></span><span class="name">我的笔记</span>
                            </div>
                            <ul class="notebook-sub">
                                <li class="notebook">
                                    <div class="info">
                                        <span class="icon fa fa-angle-right"></span><span class="name">子笔记1层</span>
                                    </div>
                                    <ul class="notebook-sub">
                                        <li class="notebook">
                                            <div class="info">
                                                <span class="icon empty"></span><span class="name">子笔记2层</span>
                                            </div>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <li class="notebook">
                            <div class="info"><span class="icon empty"></span><span class="name">回收站</span></div>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="menu">
                <div class="menu-heading">
                    <span class="menu-icon fa fa-tags"></span>
                    <span class="menu-title">标签</span>
                </div>
                <div class="menu-body">
                    <ul id="tagList">
                        <script>
                            $.post("<c:url value="/tag/list"/>", function (tagList) {
                                var html = [];
                                tagList.forEach(function (tag) {
                                    html.push("<li class='tag' style='background-color:"+tag['tagColor']+"'>"+tag['tagName']+"</li>\n");
                                });
                                $("#tagList").append($(html.join("")));
                            });
                        </script>
                        <li class="tag" style="background: red;">重要</li>
                        <li class="tag" style="background: red;">ba83j</li>
                        <li class="tag" style="background: red;">bjia</li>
                        <li class="tag" style="background: red;">bjasi</li>
                        <li class="tag" style="background: red;">重要</li>
                        <li class="tag" style="background: greenyellow;">知识</li>
                        <li class="tag" style="background: blueviolet;">linux</li>
                        <li class="tag" style="background: greenyellow;">知识</li>
                        <li class="tag" style="background: blueviolet;">linux</li>
                        <li class="tag" style="background: greenyellow;">知识</li>
                        <li class="tag" style="background: blueviolet;">linux</li>
                        <li class="tag" style="background: greenyellow;">知识</li>
                        <li class="tag" style="background: blueviolet;">linux</li>
                        <li class="tag" style="background: greenyellow;">知识</li>
                        <li class="tag" style="background: blueviolet;">linux</li>
                    </ul>
                </div>
            </div>
            <div class="menu">
                <div class="menu-heading">
                    <span class="menu-icon fa fa-share-alt"></span>
                    <span class="menu-title">分享</span>
                </div>
                <div class="menu-body">
                    <br><br>
                    albkjalksdjflakj
                </div>
            </div>
        </div>
    </div>
    <div id="notebookSplitter"></div>
    <div id="noteAndEditor"></div>
</div>
</body>
</html>
