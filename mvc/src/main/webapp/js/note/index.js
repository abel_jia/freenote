$(function () {
    var $body = $("body");
    var $notebookSplitter = $("#notebookSplitter");
    var $notebooks = $("#notebooks");
    var $noteAndEditor = $("#noteAndEditor");

    $notebookSplitter.on("mousedown", function () {
        $notebookSplitter.addClass("moving");
        $body.on("mousemove", function (e) {
            var left = e.pageX - 3;
            $notebooks.width(left);
            $notebookSplitter.css("left", left);
            $noteAndEditor.css("left", left + 5);
        });
        $body.on("mouseup", function () {
            $notebookSplitter.removeClass("moving");
            $body.off("mousemove");
        });
    });

    $notebooks.find(".menu-heading").on("click", function () {
        $(this).closest('.menu').toggleClass("open").siblings('.menu').removeClass("open");
    });

    $(".notebook>.info>.icon").on("click", function () {
        $(this).closest('.notebook').toggleClass("open");
        $(this).toggleClass("fa-angle-right fa-angle-down");
    });

});