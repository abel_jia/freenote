$.validator.setDefaults({
    debug: true,
    errorElement: 'p',
    errorClass: 'help-block',
    highlight: function (element) {
        $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
    },
    success: function (label) {
        label.closest('.form-group').removeClass('has-error').addClass("has-success");
        label.remove();
    },
    errorPlacement: function (error, element) {
        element.closest('.form-group').append(error);
    }
});
