package cn.jfstu.freenote.pojo;

/**
 * @author 贾俊峰
 * @version 1.0
 * @date 2016/11/28 18:16
 */
public class RedisConfig {
    //===============开发环境配置
    private static final String DEV_HOST = "127.0.0.1";
    private static final int DEV_PORT = 6379;
    private static final String DEV_PASSWORD = "RZILN6GkfHU6cQuJDED7euQ54Lj3oLPk";

    //===============测试环境配置
    private static final String TEST_HOST = "127.0.0.1";
    private static final int TEST_PORT = 6379;
    private static final String TEST_PASSWORD = "RZILN6GkfHU6cQuJDED7euQ54Lj3oLPk";

    //===============生产环境配置
    private static final String PRO_HOST = "127.0.0.1";
    private static final int PRO_PORT = 6379;
    private static final String PRO_PASSWORD = "RZILN6GkfHU6cQuJDED7euQ54Lj3oLPk";

    /**redis连接地址*/
    private String host;
    /**redis连接端口*/
    private int port;
    /**redis连接密码*/
    private String password;

    public RedisConfig(RuntimeEnvironment runtimeEnvironment) {
        switch(runtimeEnvironment) {
            case DEVELOPMENT:
                this.host = DEV_HOST;
                this.port = DEV_PORT;
                this.password = DEV_PASSWORD;
                break;
            case TEST:
                this.host = TEST_HOST;
                this.port = TEST_PORT;
                this.password = TEST_PASSWORD;
                break;
            case PRODUCTION:
                this.host = PRO_HOST;
                this.port = PRO_PORT;
                this.password = PRO_PASSWORD;
                break;
            default:
                throw new RuntimeException("param runtimeEnvironment does not exist");
        }
    }

    public String getHost() {
        return host;
    }

    public int getPort() {
        return port;
    }

    public String getPassword() {
        return password;
    }
}
