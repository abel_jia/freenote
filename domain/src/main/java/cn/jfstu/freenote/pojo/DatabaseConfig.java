package cn.jfstu.freenote.pojo;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 数据库配置
 *
 * @author 贾俊峰
 * @version 1.0
 * @date 2016/11/28 17:51
 */
public class DatabaseConfig {
    private static final Logger logger = LoggerFactory.getLogger(DatabaseConfig.class);

    //================ 开发环境配置
    private static final String DEV_DRIVER_NAME = "com.mysql.jdbc.Driver";
    private static final String DEV_URL = "jdbc:mysql://localhost:3306/freenote?characterEncoding=utf8&allowMultiQueries=true&useSSL=true";
    private static final String DEV_USERNAME = "root";
    private static final String DEV_PASSWORD = "EedIhxcjrTsxuOR7pQIFMQsfr89UecP/8rAlotU/2OFl06c8lHTlgA/KVL/giiJCRoO9LhraSyWybQt+Bo+dbA==";
    private static final String DEV_PUBLIC_KEY = "MFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBAMCyieukyCYicYSt6sIS2eGky6fBlNQhHoex/6pXUHu9+EwWUXp+NsWLqWfffduwn19CnylPPj12YniL8qrYbe8CAwEAAQ==";

    //================ 测试环境配置
    private static final String TEST_DRIVER_NAME = "com.mysql.jdbc.Driver";
    private static final String TEST_URL = "jdbc:mysql://localhost:3306/freenote?characterEncoding=utf8&allowMultiQueries=true&useSSL=true";
    private static final String TEST_USERNAME = "root";
    private static final String TEST_PASSWORD = "EedIhxcjrTsxuOR7pQIFMQsfr89UecP/8rAlotU/2OFl06c8lHTlgA/KVL/giiJCRoO9LhraSyWybQt+Bo+dbA==";
    private static final String TEST_PUBLIC_KEY = "MFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBAMCyieukyCYicYSt6sIS2eGky6fBlNQhHoex/6pXUHu9+EwWUXp+NsWLqWfffduwn19CnylPPj12YniL8qrYbe8CAwEAAQ==";

    //================ 生产环境配置
    private static final String PRO_DRIVER_NAME = "com.mysql.jdbc.Driver";
    private static final String PRO_URL = "jdbc:mysql://localhost:3306/freenote?characterEncoding=utf8&allowMultiQueries=true&useSSL=true";
    private static final String PRO_USERNAME = "root";
    private static final String PRO_PASSWORD = "EedIhxcjrTsxuOR7pQIFMQsfr89UecP/8rAlotU/2OFl06c8lHTlgA/KVL/giiJCRoO9LhraSyWybQt+Bo+dbA==";
    private static final String PRO_PUBLIC_KEY = "MFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBAMCyieukyCYicYSt6sIS2eGky6fBlNQhHoex/6pXUHu9+EwWUXp+NsWLqWfffduwn19CnylPPj12YniL8qrYbe8CAwEAAQ==";

    /** 驱动名称 */
    private String driverName;
    /** 数据库访问地址 */
    private String url;
    /** 数据库登录用户名 */
    private String username;
    /** 数据库连接密码（加密） */
    private String password;
    /** 数据库密码解密公钥 */
    private String publicKey;

    public DatabaseConfig(RuntimeEnvironment runtimeEnvironment) {
        logger.info("DatabaseConfig");
        switch (runtimeEnvironment) {
            case DEVELOPMENT:
                this.driverName = DEV_DRIVER_NAME;
                this.url = DEV_URL;
                this.username = DEV_USERNAME;
                this.password = DEV_PASSWORD;
                this.publicKey = DEV_PUBLIC_KEY;
                break;
            case TEST:
                this.driverName = TEST_DRIVER_NAME;
                this.url = TEST_URL;
                this.username = TEST_USERNAME;
                this.password = TEST_PASSWORD;
                this.publicKey = TEST_PUBLIC_KEY;
                break;
            case PRODUCTION:
                this.driverName = PRO_DRIVER_NAME;
                this.url = PRO_URL;
                this.username = PRO_USERNAME;
                this.password = PRO_PASSWORD;
                this.publicKey = PRO_PUBLIC_KEY;
                break;
            default:
                throw new RuntimeException("param runtimeEnvironment does not exist");

        }
    }

    public String getDriverName() {
        return driverName;
    }

    public String getUrl() {
        return url;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getPublicKey() {
        return publicKey;
    }
}
