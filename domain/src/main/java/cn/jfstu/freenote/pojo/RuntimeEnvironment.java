package cn.jfstu.freenote.pojo;

/**
 * 运行环境枚举类型，用来标识当前系统运行时所处的环境
 *
 * @author 贾俊峰
 * @version 1.0
 * @date 2016/11/28 17:44
 */
public enum RuntimeEnvironment {
    /** 开发环境 */
    DEVELOPMENT,
    /** 测试环境 */
    TEST,
    /** 生产环境 */
    PRODUCTION
}
