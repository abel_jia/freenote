package cn.jfstu.freenote.exception.extend;


import cn.jfstu.freenote.exception.base.BaseException;

/**
 * 应用异常
 * @author 贾俊峰
 * @version 1.0
 * @date 2016/9/1 9:43
 */
public class AppException extends BaseException {
    private static final long serialVersionUID = -7714171103587825898L;

    public AppException(String code) {
        setCode(code);
    }

    @Override
    public String getMessage() {
        return getCode();
    }
}
