package cn.jfstu.freenote.exception.message;

/**
 * 异常码
 *
 * @author 贾俊峰
 * @version 1.0
 * @date 2016/12/27 16:58
 */
public class ExceptionCode {
    //=========系统(system)
    /** 成功 */
    public static final String SUCCESS       = "0";
    /** 系统错误 */
    public static final String SYSTEM_ERROR  = "1";
    /** 网络错误 */
    public static final String NETWORK_ERROR = "2";
    /** 未知错误 */
    public static final String UNKNOWN_ERROR = "3";

    //========身份验证(auth)
    /** 用户名不存在 */
    public static final String USERNAME_NOT_EXIST  = "1001";
    /** 密码错误 */
    public static final String PASSWORD_MISTAKE    = "1002";
    /** 尚未登录 */
    public static final String NO_LOGIN            = "1003";
    /** 用户名已存在 */
    public static final String USER_ALREADY_EXIST  = "1004";
    /** email已存在 */
    public static final String EMAIL_ALREADY_EXIST = "1005";
}
