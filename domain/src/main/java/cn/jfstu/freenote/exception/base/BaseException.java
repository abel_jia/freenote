package cn.jfstu.freenote.exception.base;

/**
 * 异常基础类
 * @author 贾俊峰
 * @version 1.0
 * @date 2016/9/1 9:43
 */
public abstract class BaseException extends RuntimeException {
    private static final long serialVersionUID = -7576477848208902415L;

    private String code;
    public abstract String getMessage();

    public String getCode() {
        return code;
    }
    public void setCode(String code) {
        this.code = code;
    }
}
