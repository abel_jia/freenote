package cn.jfstu.freenote.exception.extend;


import cn.jfstu.freenote.exception.base.BaseException;
import cn.jfstu.freenote.exception.message.ExceptionCode;

/**
 * 系统异常
 * @author 贾俊峰
 * @version 1.0
 * @date 2016/9/1 9:44
 */
public class SysException extends BaseException {
    private static final long serialVersionUID = 4151939799004602769L;

    public SysException() {
        this(ExceptionCode.SYSTEM_ERROR);
    }

    public SysException(String code) {
        setCode(code);
    }

    @Override
    public String getMessage(){
        return getCode();
    }
}
