package cn.jfstu.freenote.entity.base;

import java.io.Serializable;

/**
 * 抽象实体类基类，所有表示数据库中数据的类都应该继承该类
 * @author 贾俊峰
 * @version 1.0
 * @date 2016/11/25 9:43
 */
public abstract class Entity implements Serializable {
}
