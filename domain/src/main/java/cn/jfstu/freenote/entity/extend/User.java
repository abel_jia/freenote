package cn.jfstu.freenote.entity.extend;

import cn.jfstu.freenote.entity.base.Entity;
import com.alibaba.fastjson.annotation.JSONField;

import java.util.Date;

/**
 * 用户实体类
 *
 * @author 贾俊峰
 * @version 1.0
 * @date 2016/11/24 16:33
 */
public class User extends Entity {
    /** 用户编号 */
    private Integer userId;
    /** 用户名 */
    private String  username;
    /** 盐值 */
    @JSONField(serialize = false)
    private String  salt;
    /** 密码 */
    @JSONField(serialize = false)
    private String  password;
    /** email地址 */
    private String  email;
    /** 创建时间 */
    @JSONField(format = "yyyy-MM-dd")
    private Date    createTime;
    /** 更新时间 */
    @JSONField(format = "yyyy-MM-dd")
    private Date    updateTime;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        return "User{" +
                "userId=" + userId +
                ", username='" + username + '\'' +
                ", salt='" + salt + '\'' +
                ", password='" + password + '\'' +
                ", email='" + email + '\'' +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                '}';
    }
}
