package cn.jfstu.freenote.entity.base;

import com.alibaba.fastjson.annotation.JSONField;
import com.alibaba.fastjson.annotation.JSONType;

/**
 * 抽象分页基础类
 * @author 贾俊峰
 * @version 1.0
 * @date 2016/9/2 23:59
 */
@JSONType
public abstract class Pagination extends Entity {
    private static final long serialVersionUID = -9059033946680784744L;

    private final static int NO_ROW_OFFSET = 1;
    private final static int NO_ROW_LIMIT = Integer.MAX_VALUE;

    //=============分页排序字段
    /**当前页*/
    @JSONField(serialize = false)
    private Integer pageNum;
    /**每页的数量*/
    @JSONField(serialize = false)
    private Integer pageSize;
    /**是否分页*/
    @JSONField(serialize = false)
    private Boolean paging = false;
    /**排序名称*/
    @JSONField(serialize = false)
    private String sortName;
    /**正序/反序*/
    @JSONField(serialize = false)
    private String sortOrder = "DESC";
    /**搜索关键字*/
    @JSONField(serialize = false)
    private String search;

    //=============构造器
    public Pagination() {
        this.pageNum = NO_ROW_OFFSET;
        this.pageSize = NO_ROW_LIMIT;
    }
    public Pagination(int pageNum, int pageSize) {
        this.pageNum = pageNum;
        this.pageSize = pageSize;
    }

    //=============get/set
    public Integer getPageNum() {
        return pageNum;
    }
    public void setPageNum(Integer pageNum) {
        this.pageNum = pageNum;
    }
    public Integer getPageSize() {
        return pageSize;
    }
    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }
    public Boolean getPaging() {
        return paging;
    }
    public void setPaging(Boolean paging) {
        this.paging = paging;
    }
    public String getSortName() {
        return sortName;
    }
    public void setSortName(String sortName) {
        this.sortName = sortName;
    }
    public String getSortOrder() {
        return sortOrder;
    }
    public void setSortOrder(String sortOrder) {
        this.sortOrder = sortOrder;
    }
    public String getSearch() {
        return search;
    }
    public void setSearch(String search) {
        this.search = search;
    }
}
