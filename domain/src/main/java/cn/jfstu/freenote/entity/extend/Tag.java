package cn.jfstu.freenote.entity.extend;

import cn.jfstu.freenote.entity.base.Entity;

/**
 * 标签实体类
 *
 * @author 贾俊峰
 * @version 1.0
 * @date 2016/12/30 17:36
 */
public class Tag extends Entity {
    /** 标签编号 */
    private Integer tagId;
    /** 标签名称 */
    private String  tagName;
    /** 标签颜色 */
    private String  tagColor;
    /** 所属用户编号 */
    private Integer userId;

    public Integer getTagId() {
        return tagId;
    }

    public void setTagId(Integer tagId) {
        this.tagId = tagId;
    }

    public String getTagName() {
        return tagName;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }

    public String getTagColor() {
        return tagColor;
    }

    public void setTagColor(String tagColor) {
        this.tagColor = tagColor;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }
}
