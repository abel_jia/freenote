package cn.jfstu.freenote.util.string;

import java.util.Random;

/**
 * 随机数生成器
 * @author 贾俊峰
 * @version 1.0
 * @date 2016/9/2 22:58
 */
public class RandomUtil {
    public enum Type {
        /** 数字和大小写字母 */
        ALL,
        /** 仅数字 */
        NUMBER_ONLY,
        /** 仅小写字母 */
        LOWER_CASE_ONLY,
        /** 仅大写字母 */
        UPPER_CASE_ONLY,
        /** 数字和小写字母 */
        NUMBER_AND_LOWER_CASE,
        /** 数字和大写字母 */
        NUMBER_AND_UPPER_CASE,
        /** 大小写字母 */
        UPPER_AND_LOWER_CASE,
        /** 展示用数字和大小写字母*/
        ALL_FOR_SHOW
    }

    /**
     * 随机生成指6位数字
     */
    public static String getRandom(){
        return getRandomString(6, Type.NUMBER_ONLY);
    }
    
    /**
     * 生成指定位数随机字符串,指定是否区分大小写
     */
    public static String getRandomString(int length, Type type) {
        String randomStr = SEED[type.ordinal()];
        char[] randomCharArr = randomStr.toCharArray();
        Random random = new Random();
        char[] charArr = new char[length];
        for (int i=0; i<length; i++) {
            charArr[i] = randomCharArr[random.nextInt(randomCharArr.length)];
        }
        return String.copyValueOf(charArr);
    }

    private final static String[] SEED = new String[Type.values().length];
    static {
        SEED[Type.ALL.ordinal()] = "1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        SEED[Type.NUMBER_ONLY.ordinal()] = "1234567890";
        SEED[Type.LOWER_CASE_ONLY.ordinal()] = "abcdefghijklmnopqrstuvwxyz";
        SEED[Type.UPPER_CASE_ONLY.ordinal()] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        SEED[Type.NUMBER_AND_LOWER_CASE.ordinal()] = "1234567890abcdefghijklmnopqrstuvwxyz";
        SEED[Type.NUMBER_AND_UPPER_CASE.ordinal()] = "1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        SEED[Type.UPPER_AND_LOWER_CASE.ordinal()] = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        SEED[Type.ALL_FOR_SHOW.ordinal()] = "1234567890abcdefghjkmnpqrstuvwxyzABCDEFGHJKMNPQRSTUVWXYZ";
    }


    public static void main(String[] args) {
        System.out.println(getRandomString(32, Type.ALL));
    }
}
