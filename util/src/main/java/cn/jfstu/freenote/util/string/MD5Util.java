package cn.jfstu.freenote.util.string;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * 加密工具集
 *
 * @author 贾俊峰
 * @since 2015年9月9日下午1:30:11
 */
public class MD5Util {
    /**
     * MD5加密
     *
     * @author 贾俊峰
     * @since 2015年9月9日下午1:29:45
     */
    public static String md5(String plainText) {
        if (null == plainText) {
            plainText = "";
        }
        String MD5Str = "";
        try {
            // JDK 6 支持以下6种消息摘要算法，不区分大小写
            // md5,sha(sha-1),md2,sha-256,sha-384,sha-512
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(plainText.getBytes());
            byte[] bytes = md.digest();
            MD5Str = byteToString(bytes);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return MD5Str;
    }

    /**
     * SHA加密
     *
     * @author 贾俊峰
     * @since 2015年9月9日下午1:39:23
     */
    public static String sha(String plainText) {
        if (null == plainText) {
            plainText = "";
        }
        String SHAStr = "";
        try {
            MessageDigest md = MessageDigest.getInstance("SHA");
            md.update(plainText.getBytes());
            byte b[] = md.digest();
            SHAStr = byteToString(b);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return SHAStr;
    }

    /**
     * 字节数组转16进制字符串
     *
     * @author 贾俊峰
     * @since 2015年9月9日下午1:17:53
     */
    private static String byteToString(byte[] bytes) {
        int i;
        StringBuilder builder = new StringBuilder(40);
        for (byte aByte : bytes) {
            i = aByte;
            if (i < 0) i += 256;
            if (i < 16) builder.append("0");
            builder.append(Integer.toHexString(i));
        }
        return builder.toString();
    }

    //测试
    public static void main(String[] args) {
        String str = "1234567badkjfkalnbkajdkfjlkdjfjlafjlsajflaksblkalkjfdlkajdlfjkdlfjaljflka";
        String md5 = md5(str);
        String sha = sha(str);
        System.out.println("md5加密结果：" + md5);
        System.out.println("sha加密结果：" + sha);
        System.out.println("72位加密结果：" + md5 + sha);
    }
}
