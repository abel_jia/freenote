package cn.jfstu.freenote.util.string;

/**
 * 字符串工具
 *
 * @author 贾俊峰
 * @version 1.0
 * @date 2016/9/1 9:39
 */
public class StringUtil {

    /**
     * 字符串首字母转小写
     */
    public static String firstToLowerCase(String source) {
        return source.replaceFirst(source.substring(0, 1), source.substring(0, 1).toLowerCase());
    }

    /**
     * 字符串首字母转大写
     */
    public static String firstToUpperCase(String source) {
        return source.replaceFirst(source.substring(0, 1), source.substring(0, 1).toUpperCase());
    }

    /**
     * 判断字符串是否为空
     */
    public static boolean isNull(String str) {
        return str == null || str.equals("NULL") || str.equals("null") || str.equals("");
    }

}
