package cn.jfstu.freenote.util.time;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 日期工具
 *
 * @author 贾俊峰
 * @version 1.0
 * @date 2016/12/5 11:05
 */
public class DateTimeUtil {
    private static final SimpleDateFormat FORMAT_DATETIMEMS          = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS");
    private static final SimpleDateFormat FORMAT_DATETIME            = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private static final SimpleDateFormat FORMAT_DATE                = new SimpleDateFormat("yyyy-MM-dd");
    private static final SimpleDateFormat FORMAT_TIME                = new SimpleDateFormat("HH:mm:ss");
    private static final SimpleDateFormat FORMAT_DATETIMEMS_UNSYMBOL = new SimpleDateFormat("yyyyMMddHHmmssSSS");
    private static final SimpleDateFormat FORMAT_DATETIME_UNSYMBOL   = new SimpleDateFormat("yyyyMMddHHmmss");
    private static final SimpleDateFormat FORMAT_DATE_UNSYMBOL       = new SimpleDateFormat("yyyyMMdd");
    private static final SimpleDateFormat FORMAT_TIME_UNSYMBOL       = new SimpleDateFormat("HHmmss");

    private static final SimpleDateFormat FORMAT = FORMAT_DATETIME;

    public static Date getDate() {
        return new Date();
    }

    public static Date getDate(String text) {
        if (text == null || text.trim().length() == 0) {
            return null;
        }
        Date date = null;
        try {
            date = FORMAT_DATETIMEMS.parse(text);
        } catch (ParseException e) {
            try {
                date = FORMAT_DATETIME.parse(text);
            } catch (ParseException e1) {
                try {
                    date = FORMAT_DATE.parse(text);
                } catch (ParseException e2) {
                    try {
                        date = FORMAT_DATETIMEMS_UNSYMBOL.parse(text);
                    } catch (ParseException e3) {
                        try {
                            date = FORMAT_DATETIME_UNSYMBOL.parse(text);
                        } catch (ParseException e4) {
                            try {
                                date = FORMAT_DATE_UNSYMBOL.parse(text);
                            } catch (ParseException e5) {
                                try {
                                    date = FORMAT_TIME_UNSYMBOL.parse(text);
                                } catch (ParseException e6) {
                                    throw new IllegalArgumentException("日期格式不正确，无将字符串[" + text +"]转换成日期/时间");
                                }
                            }
                        }
                    }
                }
            }
        }
        return date;
    }

    /**
     * 比较两个日期间相差的天数
     *
     * @param date1 日期1
     * @param date2 日期2
     * @return 日期1到日期2间隔的时间，当日期2晚于日期1值为正，反之为负
     */
    public static int daysBetween(Date date1, Date date2) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            date1 = sdf.parse(sdf.format(date1));
            date2 = sdf.parse(sdf.format(date2));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return (int) ((date2.getTime() - date1.getTime()) / (24 * 60 * 60 * 1000));
    }

    /**
     * 按天数改变日期
     *
     * @param date 原日期
     * @param days 改变的天数（正数为增加天数，负数为减少天数）
     * @return 增加后的日期
     */
    public static Date changeDays(Date date, int days) {
        return new Date(date.getTime() + (long) days * 24 * 60 * 60 * 1000);
    }

    /**
     * 获取当天的日期
     *
     * @return 日期
     */
    public static Date getDateOfTheDay() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date today = null;
        try {
            today = sdf.parse(sdf.format(new Date()));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return today;
    }

    /**
     * 日期格式化
     *
     * @param date 日期
     * @return 格式化后的日期字符串
     */
    public static String format(Date date) {
        return FORMAT.format(date);
    }

    /**
     * 按模式进行日期格式化
     *
     * @param date    日期
     * @param pattern 模式，如：yyyy-MM-dd
     * @return 格式化后的日期字符串
     */
    public static String format(Date date, String pattern) {
        SimpleDateFormat sdf = new SimpleDateFormat(pattern);
        return sdf.format(date);
    }

    public static void main(String[] args) {
        Date date = changeDays(new Date(), -27);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        System.out.println(sdf.format(date));
    }
}

