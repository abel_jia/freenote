package cn.jfstu.freenote.dao.extend;

import cn.jfstu.freenote.entity.extend.User;
import org.springframework.stereotype.Repository;

/**
 * 用户dao层
 *
 * @author 贾俊峰
 * @version 1.0
 * @date 2016/11/24 16:34
 */
@Repository
public interface UserDao {
    /**
     * 根据用户名查询用户信息
     *
     * @param username 用户名
     * @return 用户信息
     */
    User selectByUsername(String username);

    /**
     * 插入用户信息
     *
     * @param user 用户信息
     */
    void insert(User user);
}
