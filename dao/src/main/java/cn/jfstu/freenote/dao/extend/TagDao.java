package cn.jfstu.freenote.dao.extend;

import cn.jfstu.freenote.entity.extend.Tag;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 标签dao层
 *
 * @author 贾俊峰
 * @version 1.0
 * @date 2016/12/30 17:47
 */
@Repository
public interface TagDao {
    /**
     * 查询标签列表
     *
     * @param tag 查询参数
     * @return 标签列表
     */
    List<Tag> selectList(Tag tag);
}
