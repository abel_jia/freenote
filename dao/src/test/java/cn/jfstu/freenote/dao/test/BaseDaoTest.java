package cn.jfstu.freenote.dao.test;

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Dao层测试基类
 * @author 贾俊峰
 * @version 1.0
 * @date 2016/12/27 15:13
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = DaoTestConfig.class)
public abstract class BaseDaoTest extends AbstractTransactionalJUnit4SpringContextTests {}
