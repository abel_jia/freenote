package cn.jfstu.freenote.dao.test;

import cn.jfstu.freenote.dao.extend.TagDao;
import cn.jfstu.freenote.entity.extend.Tag;
import com.alibaba.fastjson.JSON;
import org.junit.Test;

import javax.annotation.Resource;
import java.util.List;

/**
 * 标签dao层测试
 *
 * @author 贾俊峰
 * @version 1.0
 * @date 2016/12/30 17:57
 */
public class TagDaoTest extends BaseDaoTest {
    @Resource
    private TagDao tagDao;

    @Test
    public void selectListTest() {
        List<Tag> tagList = tagDao.selectList(null);
        for (Tag tag : tagList) {
            System.out.println(JSON.toJSONString(tag));
        }
    }
}
