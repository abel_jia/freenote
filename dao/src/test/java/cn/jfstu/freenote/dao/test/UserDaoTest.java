package cn.jfstu.freenote.dao.test;

import cn.jfstu.freenote.dao.extend.UserDao;
import cn.jfstu.freenote.entity.extend.User;
import org.junit.Test;

import javax.annotation.Resource;

/**
 * 用户dao层测试
 * @author 贾俊峰
 * @version 1.0
 * @date 2016/12/27 15:21
 */
public class UserDaoTest extends BaseDaoTest {
    @Resource
    private UserDao userDao;

    @Test
    public void selectByUsernameTest() {
        User user = userDao.selectByUsername("admin");
        System.out.println(user);
    }
}
