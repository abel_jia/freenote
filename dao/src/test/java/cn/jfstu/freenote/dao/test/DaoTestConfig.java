package cn.jfstu.freenote.dao.test;

import cn.jfstu.freenote.dao.conf.DaoConfig;
import cn.jfstu.freenote.pojo.RuntimeEnvironment;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;

/**
 * dao层测试配置
 * @author 贾俊峰
 * @version 1.0
 * @date 2016/12/5 16:09
 */
@Import(DaoConfig.class)
public class DaoTestConfig {
    @Bean
    public RuntimeEnvironment runtimeEnvironment() {
        return RuntimeEnvironment.DEVELOPMENT;
    }
}
