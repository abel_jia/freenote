package cn.jfstu.freenote.service.conf;

import cn.jfstu.freenote.cache.conf.CacheConfig;
import cn.jfstu.freenote.dao.conf.DaoConfig;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.context.annotation.Import;
import org.springframework.stereotype.Service;

/**
 * @author 贾俊峰
 * @version 1.0
 * @date 2016/11/25 16:53
 */
@ComponentScan(basePackages = "cn.jfstu.freenote.service", useDefaultFilters = false, includeFilters = {
        @ComponentScan.Filter(type = FilterType.ANNOTATION, value = {Service.class})
})
@Configuration
@Import({CacheConfig.class, DaoConfig.class})
public class ServiceConfig {
}
