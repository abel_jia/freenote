package cn.jfstu.freenote.service.extend;

import cn.jfstu.freenote.dao.extend.TagDao;
import cn.jfstu.freenote.entity.extend.Tag;
import cn.jfstu.freenote.service.base.BaseService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 标签服务
 * @author 贾俊峰
 * @version 1.0
 * @date 2016/12/30 18:03
 */
@Service
public class TagService extends BaseService{
    @Resource
    private TagDao tagDao;

    /**
     * 根据条件获取标签列表
     * @param tag 查询条件
     * @return 标签列表
     */
    public List<Tag> getList(Tag tag) {
        return tagDao.selectList(tag);
    }
}
