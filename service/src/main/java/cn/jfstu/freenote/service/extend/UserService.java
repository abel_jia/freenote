package cn.jfstu.freenote.service.extend;

import cn.jfstu.freenote.dao.extend.UserDao;
import cn.jfstu.freenote.entity.extend.User;
import cn.jfstu.freenote.service.base.BaseService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;

/**
 * 用户服务
 * @author 贾俊峰
 * @version 1.0
 * @date 2016/11/24 18:00
 */
@Service
public class UserService extends BaseService {
    @Resource
    private UserDao userDao;

    /**
     * 根据用户名获取用户信息
     * @param username 用户名
     * @return 用户信息
     */
    public User getByUsername(String username) {
        return userDao.selectByUsername(username);
    }

    /**
     * 新增用户
     * @param user
     */
    public void add(User user) {
        user.setCreateTime(new Date());
        userDao.insert(user);
    }
}
