package cn.jfstu.freenote.cache.conf;

import cn.jfstu.freenote.pojo.RedisConfig;
import cn.jfstu.freenote.pojo.RuntimeEnvironment;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;
import redis.clients.jedis.JedisPoolConfig;

import javax.annotation.Resource;

/**
 * 缓存配置
 * @author 贾俊峰
 * @version 1.0
 * @date 2016/11/28 10:23
 */
@ComponentScan(basePackages = "cn.jfstu.freenote.cache", useDefaultFilters = false, includeFilters = {
        @ComponentScan.Filter(type = FilterType.ANNOTATION, value = Component.class)
})
@Configuration
public class CacheConfig {
    private static final Logger logger = LoggerFactory.getLogger(CacheConfig.class);

    @Resource
    private RuntimeEnvironment runtimeEnvironment;

    @Bean
    public StringRedisTemplate stringRedisTemplate() {
        logger.info("注入redis模板");
        RedisConfig redisConfig = new RedisConfig(runtimeEnvironment);

        JedisConnectionFactory jedisConnectionFactory = new JedisConnectionFactory();
        jedisConnectionFactory.setHostName(redisConfig.getHost());
        jedisConnectionFactory.setPort(redisConfig.getPort());
        jedisConnectionFactory.setPassword(redisConfig.getPassword());
        jedisConnectionFactory.setTimeout(1800);
        jedisConnectionFactory.setPoolConfig(new JedisPoolConfig());
        jedisConnectionFactory.setUsePool(true);
        jedisConnectionFactory.setDatabase(0);
        jedisConnectionFactory.afterPropertiesSet();

        StringRedisTemplate stringRedisTemplate = new StringRedisTemplate();
        stringRedisTemplate.setConnectionFactory(jedisConnectionFactory);
        return stringRedisTemplate;
    }
}
