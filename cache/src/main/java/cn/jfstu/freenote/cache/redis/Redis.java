package cn.jfstu.freenote.cache.redis;

import com.alibaba.fastjson.JSON;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.*;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * Redis工具类
 * @author 贾俊峰
 * @version 1.0
 * @date 2016/9/18 16:56
 */
@Component
public class Redis {
    private final StringRedisTemplate template;

    private ValueOperations<String, String> valueOps;
    private ListOperations<String, String> listOps;
//    private SetOperations<String, String> setOps;
    private ZSetOperations<String, String> zSetOps;
//    private HyperLogLogOperations<String, String> hllOps;
    private HashOperations<String, String, String> hashOps;

    @Autowired
    public Redis(StringRedisTemplate template) {
        this.template = template;
        this.valueOps = template.opsForValue();
        this.listOps = template.opsForList();
//        this.setOps = template.opsForSet();
//        this.zSetOps = template.opsForZSet();
//        this.hllOps = template.opsForHyperLogLog();
        this.hashOps = template.opsForHash();
    }


    //*****************************全局*****************************//
    /**
     * 获取redis库中的键列表
     * @param pattern 模式（包含* 、？等组成的字符串）
     * @return 匹配的键列表
     */
    public Set<String> keys(String pattern) {
        return template.keys(pattern);
    }

    /**
     * 删除redis中保存的键
     * @param keys 键或键组
     */
    public void del(String... keys) {
        if (keys.length == 1) {
            template.delete(keys[0]);
        } else {
            Set<String> set = new HashSet<>();
            Collections.addAll(set, keys);
            template.delete(set);
        }
    }

    /**
     * 重命名键
     * @param oldName 旧键名
     * @param newName 新键名
     */
    public void rename(String oldName, String newName) {
        template.rename(oldName, newName);
    }

    /**
     * 设置键失效时间
     * @param key 键
     * @param timeout 失效时间(秒)
     */
    public void expire(String key, long timeout) {
        template.expire(key, timeout, TimeUnit.SECONDS);
    }


    //*****************************key-value*****************************//
    /**
     * 向redis中存入对象
     * @param key 键
     * @param value 值
     */
    public void set(String key, Object value) {
        valueOps.set(key, JSON.toJSONString(value));
    }

    /**
     * 向redis中存入有生命周期的对象
     * @param key 键
     * @param value 值
     * @param seconds 有效时间（秒）
     */
    public void set(String key, Object value, long seconds) {
        valueOps.set(key, JSON.toJSONString(value), seconds, TimeUnit.SECONDS);
    }

    /**
     * 获取redis中保存的字符串
     * @param key 键
     * @return 获取到的字符串
     */
    public String get(String key) {
        return valueOps.get(key);
    }

    /**
     * 获取redis中保存的对象
     * @param key 键
     * @param clazz 对象类型
     * @param <T> 对象类名
     * @return 获取到的对象
     */
    public <T> T get(String key, Class<T> clazz) {
        return JSON.parseObject(valueOps.get(key), clazz);
    }

    //*****************************list*****************************//
    /**
     * 向redis列表头部添加对象
     * @param key 键
     * @param value 值
     * @return 列表中元素个数
     */
    public Long listLeftPush(String key, Object value) {
        return listOps.leftPush(key, JSON.toJSONString(value));
    }

    /**
     * 向redis列表头部批量添加对象
     * @param key 键
     * @param values 对象组
     * @return 添加的对象数量
     */
    public Long listLeftPushAll(String key, Object... values) {
        String[] arr = new String[values.length];
        for (int i=0; i<values.length; i++) {
            arr[i] = JSON.toJSONString(values[i]);
        }
        return listOps.leftPushAll(key, arr);
    }

    /**
     * 向redis列表头部批量添加对象
     * @param key 键
     * @param values 对象组
     * @return 添加的对象数量
     */
    public Long listLeftPushAll(String key, Collection<Object> values) {
        List<String> list = new ArrayList<>();
        for (Object obj : values) {
            list.add(JSON.toJSONString(obj));
        }
        return listOps.leftPushAll(key, list);
    }

    /**
     * 向redis列表尾部添加对象
     * @param key 键
     * @param value 值
     * @return 列表中元素个数
     */
    public Long listRightPush(String key, Object value) {
        String valueStr = JSON.toJSONString(value);
        return listOps.rightPush(key, valueStr);
    }

    /**
     * 向redis列表尾部批量添加对象
     * @param key 键
     * @param values 对象组
     * @return 添加的对象数量
     */
    public Long listRightPushAll(String key, Object... values) {
        String[] arr = new String[values.length];
        for (int i=0; i<values.length; i++) {
            arr[i] = JSON.toJSONString(values[i]);
        }
        return listOps.rightPushAll(key, arr);
    }

    /**
     * 向redis列表尾部批量添加对象
     * @param key 键
     * @param values 对象组
     * @return 添加的对象数量
     */
    public Long listRightPushAll(String key, Collection<Object> values) {
        List<String> list = new ArrayList<>();
        for (Object obj : values) {
            list.add(JSON.toJSONString(obj));
        }
        return listOps.rightPushAll(key, list);
    }

    /**
     * 移除并返回列表头部元素
     * @param key 键
     * @return 列表头部元素
     */
    public String listLeftPop(String key) {
        return listOps.leftPop(key);
    }

    /**
     * 移除并返回列表头部对象
     * @param key 键
     * @param clazz 对象所属类
     * @param <T> 对象类
     * @return 列表头部对象
     */
    public <T> T listLeftPop(String key, Class<T> clazz) {
        return JSON.parseObject(listOps.leftPop(key), clazz);
    }

    /**
     * 移除并返回列表尾部元素
     * @param key 键
     * @return 列表尾部元素
     */
    public String listRightPop(String key) {
        return listOps.rightPop(key);
    }

    /**
     * 移除并返回列表尾部对象
     * @param key 键
     * @param clazz 对象所属类
     * @param <T> 对象类
     * @return 列表尾部对象
     */
    public <T> T listRightPop(String key, Class<T> clazz) {
        return JSON.parseObject(listOps.rightPop(key), clazz);
    }

    /**
     * 返回列表索引处元素
     * @param key 键
     * @param index 索引
     * @return 索引处元素
     */
    public String listIndex(String key, long index) {
        return listOps.index(key, index);
    }

    /**
     * 返回列表索引处对象
     * @param key 键
     * @param index 索引
     * @return 索引处对象
     */
    public <T> T listIndex(String key, long index, Class<T> clazz) {
        return JSON.parseObject(listOps.index(key, index), clazz);
    }

    /**
     * 返回列表大小
     * @param key 键
     * @return 列表大小
     */
    public Long listSize(String key) {
        return listOps.size(key);
    }

    /**
     * 从列表中移除指定对象
     * @param key 键
     * @param i 最多移除个数
     * @param value 要移除的对象
     * @return 移除的个数
     */
    public Long listRemove(String key, long i, Object value) {
        return listOps.remove(key, i, JSON.toJSONString(value));
    }

    //*****************************hash*****************************//
    /**
     * 向哈希表中添加对象
     * @param key 键
     * @param hashKey 哈希键
     * @param value 要添加的对象
     */
    public void hashPut(String key, String hashKey, Object value) {
        hashOps.put(key, hashKey, JSON.toJSONString(value));
    }

    /**
     * 批量添加对象到哈希表
     * @param key 键
     * @param hashMap map键值对
     */
    public void hashPutAll(String key, Map<String, Object> hashMap) {
        Map<String, String> map = new HashMap<>();
        Set<String> set = hashMap.keySet();
        for (String hashKey : set) {
            map.put(hashKey, JSON.toJSONString(hashMap.get(hashKey)));
        }
        hashOps.putAll(key, map);
    }

    /**
     * 获取哈希表中的元素
     * @param key 键
     * @param hashKey 值
     * @return 获取到的元素
     */
    public String hashGet(String key, String hashKey) {
        return hashOps.get(key, hashKey);
    }

    /**
     * 获取哈希表中的对象
     * @param key 键
     * @param hashKey 值
     * @param clazz 对象类
     * @param <T> 对象
     * @return 获取到的对象
     */
    public <T> T hashGet(String key, String hashKey, Class<T> clazz) {
        return JSON.parseObject(hashOps.get(key, hashKey), clazz);
    }

    /**
     * 获取哈希表中对应键下的所有元素
     * @param key 键
     * @return 获取到的元素列表
     */
    public List<String> hashValues(String key) {
        return hashOps.values(key);
    }

    /**
     * 获取哈希表中对应键下的所有对象
     * @param key 键
     * @param clazz 对象类
     * @param <T> 对象
     * @return 获取到的对象列表
     */
    public <T> List<T> hashValues(String key, Class<T> clazz) {
        List<String> stringList = hashOps.values(key);
        List<T> list = new ArrayList<>();
        for (String s : stringList) {
            list.add(JSON.parseObject(s, clazz));
        }
        return list;
    }

    /**
     * 删除哈希表中对应一个或多个哈希键
     * @param key 键
     * @param hashKey 哈希键
     * @return 删除的哈希键数量
     */
    public Long hashDel(String key, String... hashKey) {
        return hashOps.delete(key, (Object[]) hashKey);
    }

    /**
     * 获取哈希表中哈希键数量
     * @param key 键
     * @return 键数量
     */
    public Long hashSize(String key) {
        return hashOps.size(key);
    }

    /**
     * 获取哈希表中所有的哈希键
     * @param key 键
     * @return 哈希键set
     */
    public Set<String> hashKeys(String key) {
        return hashOps.keys(key);
    }

}
